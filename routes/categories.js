const express = require('express');
const router = express.Router();

const CategoriesModel = require('../libs/mongoose').CategoriesModel;


router.get('/:id', (req, res) => {
  CategoriesModel.find({ parent_id: req.params.id }, (err, result) => {
    if (!err) {
      return res.send(result);
    }
    res.statusCode = 500;
    return res.send({ error: 'Server error' });
  });
});

module.exports = router;
