const express = require('express');
const router = express.Router();

const UsersModel = require('../libs/mongoose').UsersModel;

router.post('/', (req, res) => {
  const user = new UsersModel(req.body);
  user.save((error) => {
    if (!error) {
      return res.send({ status: 'OK', user });
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;      
        res.send({ error: 'Validation error' });
      } else {
        res.statusCode = 500;
        res.send({ error: 'Server error' });
      }
    }
  });
});

module.exports = router;
