const express = require('express');
const router = express.Router();

const ProductsModel = require('../libs/mongoose').ProductsModel;


router.get('/:id', (req, res) => {
  ProductsModel.find({ category_id: req.params.id }, (err, products) => {
    if (!err) {
      return res.send(products);
    }
    res.statusCode = 500;
    return res.send({ error: 'Server error' });
  });
});

router.get('/product/:id', (req, res) => {
  ProductsModel.find({ _id: req.params.id }, (err, product) => {
    if (!err) {
      return res.send(product[0]);
    }
    res.statusCode = 500;
    return res.send({ error: 'Server error' });
  });
});

module.exports = router;
