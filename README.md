# README #

###Для запуска сервера и БД необходимо:
 * склонировать репозиторий **git clone git@bitbucket.org:AlexanderLEV/shop-server.git**
 * **npm install**
 * запустить БД (--dbpath должен указывать на папку **data**, которая лежит на вашем сервере. Мой путь будет выглядеть так:  **%username:~$ mongod --dbpath Alexander/shop-server/data/**
 * запустить сервер **./node_modules/.bin/nodemon**