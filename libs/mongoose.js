const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/shopDB');
const db = mongoose.connection;

db.on('error', (err) => {
  console.log(err);
});
db.once('open', () => {
  console.log('Connect to BD!');
});

const Schema = mongoose.Schema;

const categories = new Schema({
  _id: { type: Number, required: true },
  title: { type: String, required: true },
  parent_id: { type: String, required: true }
});

const products = new Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true },
  category_id: { type: String, required: true }
});

const users = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true }
}, { versionKey: false });

const CategoriesModel = mongoose.model('categories', categories);
const ProductsModel = mongoose.model('products', products);
const UsersModel = mongoose.model('users', users);

module.exports.CategoriesModel = CategoriesModel;
module.exports.ProductsModel = ProductsModel;
module.exports.UsersModel = UsersModel;
